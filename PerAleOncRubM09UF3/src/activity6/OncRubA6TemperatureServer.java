package activity6;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class OncRubA6TemperatureServer {
	
		
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
				
		// Get the multicast IP address object of the group from the multicast IP in string format specified through the main method arguments.
		InetAddress multiCastIP = InetAddress.getByName(args[0]);
		int multiCastPort = Integer.parseInt(args[1]);
		
		// Open an UDP socket on the port indicated in the main method arguments
		DatagramSocket socket = new DatagramSocket();
		
		//Create an instance of the temperature probe provided in the activity (TemperatureProbe.java)
		OncRubA6TemperatureProbe tempProbe  =  new OncRubA6TemperatureProbe(5);
		
		//Create an ScheduledExecutorService with a single thread (UF2):
		ScheduledExecutorService schExService = Executors.newSingleThreadScheduledExecutor();
		
		//Loop de 10 min
		Date date = new Date();
		long startTime = date.getTime(); 
		long currentTime =startTime; 
		while(currentTime<startTime+10000){
			currentTime = date.getTime();
			
			//Create an empty buffer 
	    	byte[] buf = new byte[256];
	    	
	    	// Run the probe task after 5 seconds.	    	
	    	ScheduledFuture<byte[]> scheduledFuture = schExService.schedule(tempProbe, 5, TimeUnit.SECONDS);
	    	
	    	// Get data from the probe after the 5 seconds.
	    	// Modify the empty buffer with the information to be sent to the client in the packet
	    	buf= scheduledFuture.get();
	    	 
	    	//Create a data packet to be sent to the group (IP and port indicated in the arguments. The port is the same used by the MulticastSocket in the client.
	        DatagramPacket packet  = new DatagramPacket(buf, buf.length,multiCastIP,multiCastPort);
	         
	        // Send the packet to the group.
	        socket.send(packet);

		}
		socket.close();
	}
}
