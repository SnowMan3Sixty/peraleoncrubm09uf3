package activity6;

import java.util.Random;
import java.util.concurrent.Callable;

public class OncRubA6TemperatureProbe implements Callable<byte[]> {
	
	public static final int maximumBias = 10;
	public static final double maximumVariation = 0.1;
	int [] averageTemp = {6, 0, 7, 15, 18, 23, 30, 30, 25, 15, 12, 8};
	double temp;
	int month;
	long millis;
	Random random;

	public OncRubA6TemperatureProbe(int m) {
		month = m-1;
		temp = averageTemp[month];
		millis=System.currentTimeMillis();
		random = new Random(millis);
	}
	
	public byte[] call() {
		int temp = getTemperature();
		return new String(temp+"").getBytes();
	}
	
	protected int getTemperature(){

		millis = System.currentTimeMillis();
		temp += (random.nextBoolean()?1:-1)*maximumVariation*temp*random.nextFloat();
	 
		double dif = temp-averageTemp[month];
		if(dif>maximumBias){
			temp=averageTemp[month]+maximumBias;
		}
		else if(dif<-maximumBias){
			temp=averageTemp[month]-maximumBias;
		}
		return (int) temp;
	 }
}
