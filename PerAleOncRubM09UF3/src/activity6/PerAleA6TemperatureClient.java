package activity6;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;


public class PerAleA6TemperatureClient {
	public static void main(String[] args) throws IOException {
	
		// 1. Open an UDP Multicast socket on the port indicated in the main method arguments. → socket
		MulticastSocket socket = new MulticastSocket(Integer.parseInt(args[0]));
		
		// NEW in this activity: Indicate the time to wait for data to receive → 5s at most.
		socket.setSoTimeout(5000);
		
		// 2. Get the multicast IP address object of the group from the multicast IP in string format (obtained from the main method arguments). → multicastIP.
		InetAddress address = InetAddress.getByName(args[1]);
		
		// 3. Join the group (so as to be able to receive packets send to the group)
		socket.joinGroup(address);
		
		int timeouts = 0; // Inicialitze a variable to count the number of time outs.
		
		DatagramPacket packet;
		
		while(timeouts<5) {  // LOOP: While the number of time outs doesn’t reach 5
			
			try {
				
		     // 4. Create an empty buffer (byte[]).
			 byte[] buf = new byte[256];
			 
			 // 5. Create a data packet with an empty buffer to
			 packet = new DatagramPacket(buf, buf.length);
			 
			 // 6. Receive a packet SENT TO THE GROUP from the server.
			 socket.receive(packet);
			 
			 // 7. Get the received information (byte[]) from the packet
			 String received = new String(packet.getData(), 0, packet.getLength());
			 
			 // Print the received information
			 System.out.println("The actual temperature is: " + received + " degrees.");
			 
			}catch(SocketTimeoutException s) {
				
				// Update the number of time outs
				timeouts+=1;
				
				System.out.println("Socket timed out!");  
			}
			
		}
		
		//8. Leave the group.
		socket.leaveGroup(address);
		
	    //9. Close the socket.
	    socket.close();
	}
}
