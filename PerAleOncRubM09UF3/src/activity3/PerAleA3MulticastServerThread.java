package activity3;

/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
import java.io.*;
import java.net.*;
import java.util.*;

import activity2.OncRubA2QuoteServerThread;
 
public class PerAleA3MulticastServerThread extends OncRubA2QuoteServerThread {
 
    private long FIVE_SECONDS = 5000;
    private int port;
    private String ip;
 
    public PerAleA3MulticastServerThread(int port,String ip) throws IOException {
        super("MulticastServerThread",port);												//10. Open an UDP socket. How is it open? Is the socket type the same than for the client?
        this.port=port;
        this.ip=ip;
    }
 
    public void run() {
        while (moreQuotes) {
            try {
                byte[] buf = new byte[256];													//11. Create an empty buffer (byte[]).
 
                    // construct quote
                String dString = null;
                if (in == null)
                    dString = new Date().toString();
                else
                    dString = getNextQuote();
                buf = dString.getBytes();													//12. Modify the empty buffer with the information to be sent to the group in the packet.
 
            // send it
                InetAddress group = InetAddress.getByName(ip);							    //13. Get the multicast IP address object of the group from the multicast IP in string format. Note that this could be done just once. Where would you move this line?
                DatagramPacket packet = new DatagramPacket(buf, buf.length, group, port);	//14. Create a data packet to be sent to the group with IP ? which is assigned to the port ?. (Indicate the IP and the port number). Indicate whether this port is the same used by the MulticastSocket. 
                socket.send(packet);														//15. Send the packet to the group.
 
            // sleep for a while
        try {
            sleep((long)(Math.random() * FIVE_SECONDS));
        } catch (InterruptedException e) { }
            } catch (IOException e) {
                e.printStackTrace();
        moreQuotes = false;
            }
        }
    socket.close();																			//16.Close the socket.
    }
}