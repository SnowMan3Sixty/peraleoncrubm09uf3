package activity3;

/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
import java.io.*;
import java.net.*;
import java.util.Scanner;
 
public class OncRubA3MulticastClient {
 
    public static void main(String[] args) throws IOException {
    	String option;
    	boolean fi = false;
    	Scanner input = new Scanner(System.in);
    	
    	// 1. Open an UDP Multicast socket (required to receive info sent to a group) on port ?.
        MulticastSocket socket = new MulticastSocket(Integer.parseInt(args[0]));
        // 2. Get the multicast IP address object of the group from the multicast IP in string format.
        InetAddress address = InetAddress.getByName(args[1]);
        //3. Join the group (so as to be able to receive packets send to the group).
        socket.joinGroup(address);
 
        DatagramPacket packet;
     
            // get a few quotes
    while(!fi) {
    	//4. Create an empty buffer (byte[]).
        byte[] buf = new byte[256];
        	//5. Create a data packet with an empty buffer to receive information from the server (sent to the group)
            packet = new DatagramPacket(buf, buf.length);
            //6. Receive a packet sent to the group from the server
            socket.receive(packet);
            //7. Get the received information (byte[]) from the packet.
            String received = new String(packet.getData(), 0, packet.getLength());
            System.out.println("Quote of the Moment: " + received);
            
        System.out.println( "Do you want to get another quote (Y|N)?");
        option = input.next();
        if(option.equals("N")) {
        	fi = true;
        }
    }
    //8. Leave the group.
    socket.leaveGroup(address);
    //9. Close the socket.
    socket.close();
    
    input.close();
    }
 
}
