package activity5;

/*
 * Copyright (c) 1995, 2014, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
import java.net.*;

import activities.KnockKnockProtocol;

import java.io.*;
 
public class OncRubA5KnockKnockUDPServer {
	
	static DatagramSocket socket = null;
	static int portNumber;
	
    public static void main(String[] args) throws IOException {
    	
    	String inputLine,outputLine;    	
    	portNumber  = Integer.parseInt(args[0]);
    	
    	socket =  new DatagramSocket(portNumber);
    	
    	//ERROR message for invalid arguments
        if (args.length != 1) {
            System.err.println("Usage: java KnockKnockServer <port number>");
            System.exit(1);
        }
 
        try {
        	//Create an empty buffer
        	byte[] buf = new byte[256];
        	//Create a data packet with an empty buffer
        	DatagramPacket packet = new DatagramPacket(buf, buf.length);
        	//Receive a packet from the client
        	socket.receive(packet);
        	
        	//Initiate  conversation with the client
        	KnockKnockProtocol kkp = new KnockKnockProtocol();
        	outputLine = kkp.processInput(null);
        	//Modify the empty buffer with the information to be sent to the client in the packet.
        	buf = outputLine.getBytes();
        	//send the response to the client at "localhost" "4321"
        	
        	//get the ip address object of the client from the packet
        	InetAddress hostName = packet.getAddress();
        	//Get the port of the client. Indicate whether this port is the same used by the server.
        	int port = packet.getPort();
        	
        	packet = new DatagramPacket(buf, buf.length, hostName,port);
        	socket.send(packet);
        	        	
        	//Read from the client socket input stream.
        	while((inputLine = recieved()) != null){
        		//Server processing.
        		outputLine = kkp.processInput(inputLine);
        		send(outputLine, hostName, port);
        		if(outputLine.equals("Bye.")) {
        			break;
        		}
        	}
        	
        }catch(IOException e) {
        	System.out.println(e.getMessage());
        }finally {
        	socket.close();
        }        
    }
    
    
    public static String recieved() throws IOException {
    	//Create an empty buffer 
    	byte[] buf = new byte[256];
    	//Create a data packet with an empty buffer to receive information from the server.
    	DatagramPacket packet = new DatagramPacket(buf, buf.length);
    	//Receive a packet from the server.   	
    	socket.receive(packet);
    	//Return the response
    	String response  = new String(packet.getData(),0,packet.getLength());
    	return response;
    }
    
    public static void send(String  outputLine,InetAddress hostName, int portNumber) throws IOException {
    	//Create an empty buffer 
    	byte[] buf = new byte[256];
    	//Create a data packet with an empty buffer to receive information from the server.
    	DatagramPacket packet = new DatagramPacket(buf, buf.length);
    	//Modify the empty buffer with the information to be sent to the client in the packet.
    	buf = outputLine.getBytes();
    	//send the response to the client
    	packet = new DatagramPacket(buf, buf.length,hostName,portNumber);
    	socket.send(packet);    	
    }  
}