/*
 * Copyright (c) 1995, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package activity5;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class PerAleA5KnockKnockUDPClient {
	
	static DatagramSocket socket = null;			
		
    public static void main(String[] args) throws IOException {    	
    	
    	Scanner  input = new Scanner(System.in);
    	//1.Open an UDP socket
    	socket = new DatagramSocket();
    	
    	//ERROR message for invalid arguments
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }
        
        //Get the IP address object of the server from the host name or host IP in string format
        InetAddress hostName = InetAddress.getByName(args[0]);
        int portNumber = Integer.parseInt(args[1]);
        
        //Create an empty buffer byte[]
        byte[] buf = new byte[256];
        
        //Create a data packet to be sent to the client with IP «localhost» which is listening on port 4321)
        DatagramPacket packet  = new DatagramPacket(buf, buf.length,hostName,portNumber);
        //Send the packet to the server
        socket.send(packet);
        
        String fromServer,fromUser;
        
        while((fromServer = recieved()) != null) {
        	System.out.println("Server: " + fromServer);
        	if(fromServer.equals("Bye.")) {
        		break;
        	}
        	fromUser = input.nextLine();
        	if(fromUser != null) {
        		System.out.println("Client: "  +  fromUser);
        		send(fromUser,hostName,portNumber);
        	}
        	
        }
        //Close the scanner
        input.close();
        //Close the socket
        socket.close();
        
    }
    
    public static String recieved() throws IOException {
    	//Create an empty buffer 
    	byte[] buf = new byte[256];
    	//Create a data packet with an empty buffer to receive information from the server.
    	DatagramPacket packet = new DatagramPacket(buf, buf.length);
    	//Receive a packet from the server.   	
    	socket.receive(packet);
    	//Return the response
    	String response  = new String(packet.getData(),0,packet.getLength());
    	return response;
    }
    
    
    public static void send(String  outputLine,InetAddress hostName, int portNumber) throws IOException {
    	//Create an empty buffer 
    	byte[] buf = new byte[256];
    	//Create a data packet with an empty buffer to receive information from the server.
    	DatagramPacket packet = new DatagramPacket(buf, buf.length);
    	//Modify the empty buffer with the information to be sent to the server in the packet.
    	buf = outputLine.getBytes();
    	//send the response to the server
    	packet = new DatagramPacket(buf, buf.length,hostName,portNumber);
    	socket.send(packet);    	
    }         

}
