package activity4;

/*
 * Copyright (c) 1995, 2014, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
 
public class PerAleA4QuoteTCPServer {
    public static void main(String[] args) throws IOException {
         
        if (args.length != 1) {
            System.err.println("Usage: java KnockKnockServer <port number>");
            System.exit(1);
        }
 
        int portNumber = Integer.parseInt(args[0]);
        ServerSocket serverSocket=null;
        Socket clientSocket=null;
        PrintWriter out=null;
        BufferedReader in=null;
        try{
        	
        	serverSocket = new ServerSocket(portNumber);//9. Open the server socket to listen on port [indicate the port]
            clientSocket = serverSocket.accept();//1. Open a client socket
            System.out.println(clientSocket.getInetAddress());
            System.out.println(serverSocket.getLocalPort());
            System.out.println(clientSocket.getLocalAddress());
            System.out.println(clientSocket.getLocalPort());
            out =
                new PrintWriter(clientSocket.getOutputStream(), true);//2. Open the input stream of the client socket//5. Write to the client socket output stream. 
            in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));//3. Open the output stream to the client socket.//4. Read from the client socket input stream. 
         
            String inputLine, outputLine;
             
            while ((inputLine = in.readLine()) != null) {// 4.Read from the client socket input stream.
            	outputLine = Files.readAllLines(Paths.get("one-liners.txt")).get(Integer.parseInt(inputLine));
            	out.println(outputLine);
                if (outputLine.equals("Bye."))
                    break;
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        } finally {
        	out.close();//7. Close the output stream of the client socket.
        	in.close();//6. Close the input stream of the client socket.
        	serverSocket.close();//11. Close the server socket.
        	clientSocket.close();//8. Close the client socket. 
        }
    }
}