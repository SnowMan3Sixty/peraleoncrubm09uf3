/*
 * Copyright (c) 1995, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package activity4;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class OncRubA4QuoteTCPClient {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        
        Socket kkSocket = null;
        BufferedReader in = null;
        BufferedReader stdIn = null;
        PrintWriter out = null;
        Scanner input = null;
        
        try {
        	// 1. Open a client socket.
            kkSocket = new Socket(hostName, portNumber);
            // 3. Open the output stream to the client socket. 
            out = new PrintWriter(kkSocket.getOutputStream(), true);
            // 2. Open the input stream of the client socket.
            in = new BufferedReader( new InputStreamReader(kkSocket.getInputStream()));
            // 4. Read from the client socket input stream.
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            String fromServer;
//            String fromUser;
            
            String aux;
            int quoteNumber = -1;
            input = new Scanner(System.in);
            boolean firstCheck,secondCheck;
            
            do {
            	secondCheck = false;
            	System.out.println("Do you want to get a quote (Y/N)? ");
            	aux = input.next().toLowerCase();
            	
            	if(aux.equals("y")) {
            		do {
            			firstCheck = false;
            			try {
            				System.out.println("Which one? Write a number.");
            				quoteNumber = Integer.parseInt(input.next());
            			}catch(NumberFormatException e){
            				System.out.println("Enter a number");
            				firstCheck = true;
            			}
            		}while(firstCheck);
            		// 5. Write to the client socket output stream.
            		out.println(quoteNumber);
            		if((fromServer = in.readLine()) != null) {
            			System.out.println("Quote " + quoteNumber + ": " + fromServer);            			
            		}
            		else {
            			System.out.println("Quote " + quoteNumber +  ": Sorry, we have no such a quote");
            		}
            		
            		secondCheck  = true;
            	}
            	else if(aux.equals("n")) {
            		System.out.println("Bye");
            	}
            	else {
            		System.out.println("Enter a valid option");
            		secondCheck = true;
            	}
            	
            }while(secondCheck);            
           
        } catch (UnknownHostException e) {
        	// 12. Exception thrown to indicate that the IP address of a host (the server) could not be determined.
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }
        finally {
        	// 6. Close the input stream of the client socket. 
        	in.close();
        	// 7. Close the output stream of the client socket. 
        	out.close();
        	// 8. Close the client socket. 
        	kkSocket.close();   
        	stdIn.close();
        	input.close();
        }
    }
}
