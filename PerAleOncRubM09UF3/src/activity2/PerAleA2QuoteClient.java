/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package activity2;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class PerAleA2QuoteClient {
    @SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
    	Scanner teclat = new Scanner(System.in);
        if (args.length != 2) {
             System.out.println("Usage: java QuoteClient <hostname> <port>");
             return;
        }

            // get a datagram socket
        DatagramSocket socket = new DatagramSocket();							   //1.Open UDP Socket
        String resposta="N";
        do {
            // send request
        byte[] buf = new byte[256];												   //2.Create an empty buffer (byte[])
        InetAddress address = InetAddress.getByName(args[0]);					   //8. Get the IP address object of the server from the host name or host IP in string format.
        int port = Integer.parseInt(args[1]);
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);//3. Create a data packet to be sent to the server/client with IP ? which is listening on port ?. (Indicate the IP and the port number).
        socket.send(packet);													   //4. Send the packet to the server/client.												       
    
            // get response
        packet = new DatagramPacket(buf, buf.length);							   //5. Create a data packet with an empty buffer to receive information from the server/client.
        socket.receive(packet);													   //6. Receive a packet from the server/client. 					 
        
        
	    // display response
        String received = new String(packet.getData(), 0, packet.getLength());	   //9. Get the received information (byte[]) from the packet.
        System.out.println("Quote of the Moment: " + received);
        System.out.println("Do you want to get another quote (Y|N)?");
        resposta=teclat.next();
        }while(resposta.contentEquals("Y"));
        teclat.close();
        socket.close();															   //7. Close the socket.
    }
}
