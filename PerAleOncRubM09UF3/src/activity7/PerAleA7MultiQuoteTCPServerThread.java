package activity7;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;


import java.io.*;

public class PerAleA7MultiQuoteTCPServerThread extends Thread {
    private Socket socket = null;
    private int portNumber = 0;

    public PerAleA7MultiQuoteTCPServerThread(Socket socket,int portNumber) {
        super("PerAleA7MultiQuoteTCPServerThread");
        this.socket = socket;
        this.portNumber=portNumber;
        
    }
    
    public void run() {
    	PrintWriter out=null;
    	BufferedReader in=null;
    	 try{             
             out = new PrintWriter(socket.getOutputStream(), true);//4. Open the input stream of the client socket//8. Write to the client socket output stream. 
             in = new BufferedReader(new InputStreamReader(socket.getInputStream()));//5. Open the output stream to the client socket.
          
             String inputLine, outputLine;
              
             while ((inputLine = in.readLine()) != null) {//6. Read from the client socket input stream.
             	outputLine = Files.readAllLines(Paths.get("one-liners.txt")).get(Integer.parseInt(inputLine));
             	out.println(outputLine);//8. Write to the client socket output stream.

                 if (outputLine.equals("Bye."))
                     break;
             }
            in.close();//9. Close the input stream of the client socket.
         } catch (IOException e) {
             System.out.println("Exception caught when trying to listen on port "
                 + portNumber + " or listening for a connection");
             System.out.println(e.getMessage());
         } finally {
         	out.close();//10. Close the output stream of the client socket.
         	
         }
    }
}
