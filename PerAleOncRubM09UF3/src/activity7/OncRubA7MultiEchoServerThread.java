package activity7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class OncRubA7MultiEchoServerThread extends Thread {

    private Socket socket = null;
    private int portNumber;
    
    public OncRubA7MultiEchoServerThread(Socket socket,int portNumber) {
        super("OncRubA7MultiEchoServerThread");
        this.socket = socket;
        this.portNumber = portNumber;
    }
    
    public void run() {
    	BufferedReader in = null;
    	PrintWriter out = null;    
      
        try {   
        	//5. Open the output stream to the client socket.
            out = new PrintWriter(socket.getOutputStream(), true);     
            //4. Open the input stream of the client socket
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));         
            String inputLine;
            //6. Read from the client socket input stream.
            //7. Server processing.
            while ((inputLine = in.readLine()) != null) {
            	String outputLine=inputLine;
            	//8. Write to the client socket output stream.
                out.println(outputLine);                
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
        finally {      
        	try {
        		//9. Close the input stream of the client socket.
				in.close();
			} catch (IOException e) {				
				e.printStackTrace();
			}
        	// 10.Close the output stream of the client socket
        	out.close();
        }
    }
}

